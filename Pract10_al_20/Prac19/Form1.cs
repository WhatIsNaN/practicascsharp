﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac19
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                double HorasTrabajadas = Convert.ToDouble(textBox2.Text);
                double PrecioPorHoras = Convert.ToDouble(textBox3.Text);

                double Sueldo = HorasTrabajadas * PrecioPorHoras;

                if (checkBox1.Checked)
                    Sueldo = Sueldo - (Sueldo * 0.287);

                if (checkBox2.Checked)
                    Sueldo = Sueldo - (Sueldo * 0.304);

                textBox4.Text = Sueldo.ToString();

            }
            catch
            {
                textBox4.Text = "0";

            }
        }
    }
}
