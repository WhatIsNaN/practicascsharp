﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac16
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                double Horas = Convert.ToDouble(textBox1.Text);
                double PagoXHoras = Convert.ToDouble(textBox2.Text);

                double Sueldo = Horas * PagoXHoras;
                textBox3.Text = Sueldo.ToString();

                textBox4.Text = (Sueldo * 0.3).ToString();

                textBox5.Text = (Convert.ToDouble(textBox4.Text) + Sueldo).ToString(); 
            }
            catch
            {
                textBox3.Text = "0";
                textBox4.Text = "0";
                textBox5.Text = "0";

                MessageBox.Show("Valores Invalidos");
            }
        }
    }
}
