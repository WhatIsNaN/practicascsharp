﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac14
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var monto = Convert.ToDouble(numericUpDown1.Value);
            double descuento = 0;

            if (monto >= 100)
                descuento = 0.10;

            if (monto < 100)
                descuento = 0.02;

            textBox2.Text = (monto * descuento).ToString();
        }
    }
}
