﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Prac10
{
    public partial class Form1 : Form
    {
        DataTable data;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (!File.Exists("Data.txt"))
            {
                File.Create("Data.txt").Close();
            }

            string[] list = File.ReadAllLines("Data.txt");

            data = new DataTable();
            data.Columns.Add("Nombre");
            data.Columns.Add("Fecha");
            data.Columns.Add("Plan");
            data.Columns.Add("Precio");
            data.Columns.Add("Boletos");
            if (list.Count() != 0)
            {
                foreach (string item in list)
                {
                    string[] line = item.Split('-');
                    var dr = data.NewRow();

                    dr[0] = line[0];
                    dr[1] = line[1];
                    dr[2] = line[2];
                    dr[3] = line[3];
                    dr[4] = line[4];

                    data.Rows.Add(dr);
                }
            }

            dataGridView1.DataSource = data;

            comboBox1.Items.Add("Puerto Rico");
            comboBox1.Items.Add("Panama");
            comboBox1.Items.Add("Cuba");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length <= 0)
            {
                return;
            }

            var dr = data.NewRow();

            dr[0] = textBox1.Text;
            dr[1] = DateTime.Now.ToString();
            dr[2] = comboBox1.Text;

            var PuertoRico = 15000;
            var Panama = 25000;
            var Cuba = 34000;

            if (comboBox1.Text == "Puerto Rico")
            {
                dr[3] = (PuertoRico + (PuertoRico * 0.05)).ToString();
                if(numericUpDown1.Value > 1)
                   dr[3] = Convert.ToUInt32(dr[3]) - (PuertoRico * 0.12);

            }

            if (comboBox1.Text == "Panama")
            {
                dr[3] = (Panama + (Panama * 0.05)).ToString();
                if (numericUpDown1.Value > 1)
                    dr[3] = Convert.ToUInt32(dr[3]) - (Panama * 0.12);
            }

            if (comboBox1.Text == "Cuba")
            {
                dr[3] = (Cuba + (Cuba * 0.05)).ToString();
                if (numericUpDown1.Value > 1)
                    dr[3] = Convert.ToUInt32(dr[3]) - (Cuba * 0.12);
            }

            dr[4] = numericUpDown1.Value;

            data.Rows.Add(dr);
            dataGridView1.DataSource = data;
            
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            File.Delete("Data.txt");
         
            List<string> content = new List<string>();

            foreach (DataRow item in data.Rows)
            {
                content.Add(string.Format("{0}-{1}-{2}-{3}-{4}", item[0], item[1], item[2], item[3], item[4]));
            }

            File.AppendAllLines("Data.txt", content);

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
