﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac12
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Dictionary<string[], int> listPersonas = new Dictionary<string[], int>();
            listPersonas.Add(new string[2] { "M", "Luisa" }, 20);
            listPersonas.Add(new string[2] { "M", "Maria" }, 11);
            listPersonas.Add(new string[2] { "F", "Antonia" }, 12);
            listPersonas.Add(new string[2] { "F", "Augusta"}, 13);
            listPersonas.Add(new string[2] { "F", "Carla" }, 15);
            listPersonas.Add(new string[2] { "F", "Altagracia" }, 16);
            listPersonas.Add(new string[2] { "F", "Mircia" }, 18);

            foreach(var item in listPersonas)
            {
                if(item.Value < 20 && item.Key[0] == "F")
                {
                    listBox1.Items.Add(item.Key[1]);
                }
            }

            if (listBox1.Items.Count > (listPersonas.Count * 0.7))
            {
                MessageBox.Show("Usted ha sido agraciado de una apartamento");
                pictureBox1.BackgroundImage = Image.FromFile("apartamento.jpg");
                pictureBox1.BackgroundImageLayout = ImageLayout.Stretch;
            }

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
