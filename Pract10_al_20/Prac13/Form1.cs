﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac13
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var Sueldo = Convert.ToInt32(textBox1.Text);
                double descuento = 0;

                if (Sueldo <= 400000)
                    descuento = 1.10;

                if (Sueldo > 400000)
                    descuento = 1.05;

                if (Sueldo > 600000)
                    descuento = 1.03;

                var calculation = Convert.ToDouble(Sueldo) / descuento;

                calculation = Math.Round(calculation, 2);

                textBox2.Text = calculation.ToString();

                textBox3.Text = (Sueldo - calculation).ToString();
                
            }
            catch
            {
                MessageBox.Show("Valores Invalidos");
            }
        }
    }
}
