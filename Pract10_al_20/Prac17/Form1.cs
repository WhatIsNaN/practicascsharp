﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac17
{
    public partial class Form1 : Form
    {
        public List<string> Provincias = new List<string>();
        public Dictionary<string, double> Lugares = new Dictionary<string, double>();
        public double precioBebida;
        public double precioSnakes;
        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            precioBebida = 50;
            precioSnakes = 20;
            Provincias.Add("Santiago");
            Provincias.Add("Santo Domingo");
            Lugares.Add("palcos",800);
            Lugares.Add("ampliaciones",400);
            Lugares.Add("Bleachers",50);

            comboBox1.Items.AddRange(Provincias.ToArray());
            comboBox2.Items.AddRange(Lugares.Keys.ToArray());



        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Santiago")
                label9.ForeColor = Color.Yellow;

            if (comboBox1.Text == "Santo Domingo")
                label9.ForeColor = Color.Blue;

            comboBox2.Items.Clear();
            comboBox2.Items.AddRange(Lugares.Keys.ToArray());

            if (comboBox1.Text != "Santiago")
            {
                Dictionary<string, double> newLugares = new Dictionary<string, double>();
                newLugares.Add("palcos", 800);
                newLugares.Add("Bleachers", 50);

                comboBox2.Items.Clear();
                comboBox2.Items.AddRange(newLugares.Keys.ToArray());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try { 
            double value = Lugares[comboBox2.Text];


            if (DateTime.Now.DayOfWeek.ToString() == DayOfWeek.Sunday.ToString())
            {
                value = value + (value * 0.05);
            }

            if (numericUpDown3.Value == 3 && comboBox2.Text == "palcos")
            {
                double temp = value;
                value = value * Convert.ToDouble((numericUpDown3.Value - 1)); 
                value += temp * 0.5;
            }
            else
            {
                    value = value * Convert.ToDouble(numericUpDown3.Value);

            }

                precioBebida = precioBebida * Convert.ToDouble(numericUpDown1.Value);
                precioSnakes = precioSnakes * Convert.ToDouble(numericUpDown2.Value);


                value = precioBebida + precioSnakes + value;
            string message = string.Format("Comprador : {0},  Valor de Compra : {1}, Estadio : {2} ",
            textBox1.Text, value, comboBox1.Text);

            MessageBox.Show(message);
            }
            catch
            {
                MessageBox.Show("Uno de los Valores es Invalido.");

            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            precioBebida = 50;
            precioSnakes = 20;

            if (comboBox1.Text == "Santiago" && comboBox2.Text == "Bleachers")
            {
                precioBebida = 0;
                precioSnakes = 0;
            }

        }
    }
}
