﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac18
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length < 1 || textBox2.Text.Length < 1 || comboBox1.Text.Length < 1)
                return;

            string Articulo;

            Articulo = "El";

            if (comboBox1.Text == "Jipeta" || comboBox1.Text == "Camioneta")
                Articulo = "La";

            string Message = String.Format("{0} {1} Modelo {2} de Color {3} tiene {4} Años, {5} Dias",
                Articulo, comboBox1.Text, textBox2.Text, textBox1.Text,
                (DateTime.Now.Year - numericUpDown1.Value), 
                (DateTime.Now.Month * 30) + DateTime.Now.Day);

            label1.Text = Message;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            numericUpDown1.Maximum = DateTime.Now.Year;
        }
    }
}
