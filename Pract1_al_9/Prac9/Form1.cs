﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Prac9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] filepaths = System.IO.Directory.GetFiles("Frutas");
            List<FileInfo> fileInfo = new List<FileInfo>();
                       
            foreach(string _FILEPATH in filepaths)
            {
                fileInfo.Add(new FileInfo(_FILEPATH));
            }

            foreach(FileInfo info in fileInfo)
            {
                comboBox1.Items.Add(info.Name.Split('.')[0]);
            }



        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add(comboBox1.Text);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Image pic = Image.FromFile("Frutas/" + listBox1.Text + ".jpg");
            pictureBox1.BackgroundImageLayout = ImageLayout.Stretch;
            pictureBox1.BackgroundImage = pic;
        }
    }
}
