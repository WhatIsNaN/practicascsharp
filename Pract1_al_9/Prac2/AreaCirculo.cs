﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac2
{
    public partial class AreaCirculo : UserControl
    {
        public AreaCirculo()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double valR = Convert.ToDouble(textBox1.Text);
            double calculation = Math.PI * (valR * valR);
            textBox4.Text = calculation.ToString();
        }
    }
}
