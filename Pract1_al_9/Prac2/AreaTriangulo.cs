﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac2
{
    public partial class AreaTriangulo : UserControl
    {
        public AreaTriangulo()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double valB = Convert.ToDouble(textBox1.Text);
            double valH = Convert.ToDouble(textBox2.Text);
            double calculation = (1 / 2) * (valB * valH);

            textBox4.Text = calculation.ToString();
        }
    }
}
