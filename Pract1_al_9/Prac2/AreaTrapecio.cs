﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac2
{
    public partial class AreaTrapecio : UserControl
    {
        public AreaTrapecio()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double valA = Convert.ToDouble(textBox1.Text);
            double valB = Convert.ToDouble(textBox2.Text);
            double ValH = Convert.ToDouble(textBox3.Text);
            double calculation = (0.5) * (valA + valB) * ValH;
            textBox4.Text = calculation.ToString();
        }
    }
}
