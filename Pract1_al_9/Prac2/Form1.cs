﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Controls.Clear();
            AreaTriangulo arTri = new AreaTriangulo();
            arTri.Width = groupBox1.Width;
            groupBox1.Controls.Add(arTri);
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Controls.Clear();
            AreaCuadrado arTri = new AreaCuadrado();
            arTri.Width = groupBox1.Width;
            groupBox1.Controls.Add(arTri);
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Controls.Clear();
            AreaTrapecio arTri = new AreaTrapecio();
            arTri.Width = groupBox1.Width;
            groupBox1.Controls.Add(arTri);
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Controls.Clear();
            AreaCirculo arTri = new AreaCirculo();
            arTri.Width = groupBox1.Width;
            groupBox1.Controls.Add(arTri);
        }
    }
}
