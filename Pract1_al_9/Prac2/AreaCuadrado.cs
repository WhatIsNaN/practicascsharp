﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac2
{
    public partial class AreaCuadrado : UserControl
    {
        public AreaCuadrado()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double valA = Convert.ToDouble(textBox1.Text);
            double calculation = Math.Sqrt(valA);
            textBox4.Text = calculation.ToString();
        }
    }
}
