﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {

            textBox1.Text = String.Format("{0} ºC", hScrollBar1.Value.ToString());

            double calculation = 0;

            if (hScrollBar1.Value != 0)
                    calculation = ((9 * hScrollBar1.Value) / 5) + 32;
            
            textBox2.Text = String.Format("{0} ºF", calculation.ToString());

        }
    }
}
