﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac1
{
    public partial class CalcForm : Form
    {
        public CalcForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox3.Text = Calcular(textBox1.Text, textBox2.Text, comboBox1.Text).ToString();
        }

        public double Calcular(string v1, string v2, string Opt)
        {
            switch (Opt)
            {
                case "Suma":
                    return Convert.ToDouble(v1) + Convert.ToDouble(v2);
                case "Resta":
                    return Convert.ToDouble(v1) - Convert.ToDouble(v2);
                case "Multiplicacion":
                    return Convert.ToDouble(v1) * Convert.ToDouble(v2);
                case "Division":
                    return Convert.ToDouble(v1) / Convert.ToDouble(v2);
                default:
                    return 0;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }

        }
    }
}
