﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label4.ForeColor = Color.Blue;
           try
            {
                double valA = Convert.ToDouble(textBox1.Text);
                double valB = Convert.ToDouble(textBox2.Text);
                double valC = Convert.ToDouble(textBox3.Text);

                if (valA  > valB)
                {
                    if(valB < valC)
                    {
                        label4.Text = valB.ToString();
                        return; 
                    }

                    label4.Text = valC.ToString();
                    return;
                }

                if(valA < valC)
                {
                        label4.Text = valA.ToString();
                        return;
                }             
            }
            catch
            {
                label4.Text = "Invalido";
            }
        }
    }
}
