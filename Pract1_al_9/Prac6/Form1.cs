﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var Nombre = textBox1.Text;
            var Sexo = textBox2.Text;
            var edad = textBox3.Text;

            DateTime newTiempo = new DateTime(Convert.ToInt32(edad), 
                DateTime.Now.Month, 
                DateTime.Now.Day, 
                DateTime.Now.Hour, 
                DateTime.Now.Minute, 
                DateTime.Now.Second);

            var texto = string.Format("Mi nombre es {0}, soy {1}, tengo {2} Años"
                + ", {3} Meses, {4} Días, {5} Horas, {6} Minutos, {7} Segundos Viviendo.", 
                Nombre, Sexo, newTiempo.Year, newTiempo.Month, newTiempo.Day
                , newTiempo.Hour, newTiempo.Minute, newTiempo.Second);

            richTextBox1.Text = texto;
        }
    }
}
