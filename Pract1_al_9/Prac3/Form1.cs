﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                double One = Convert.ToDouble(textBox1.Text);
                double Two = Convert.ToDouble(textBox2.Text);

                if (One <= Two)
                {
                    double temp = One;
                    for (int x = 1; x <= Two; x++)
                    {
                        One = One * x;
                        if (One == Two)
                        {
                            One = temp;
                            textBox3.Text = String.Format("{0} es multiplo de {1}", One, Two);
                            return;

                        }
                        One = temp;
                    }
                    textBox3.Text = String.Format("{0} no es multiplo de {1}", One, Two);
                }

                if (One >= Two)
                {
                    double temp = Two;
                    for (int x = 1; x <= One; x++)
                    {
                        Two = Two * x;
                        if (Two == One)
                        {
                            Two = temp;
                            textBox3.Text = String.Format("{0} es multiplo de {1}", Two, One);
                            return;

                        }
                        Two = temp;
                    }
                    textBox3.Text = String.Format("{0} no es multiplo de {1}", Two, One);
                }
            }
            catch
            {
                textBox3.Text = "Valores Invalidos";
            }
        }
    }
}
