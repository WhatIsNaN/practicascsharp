﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var calculo = numericUpDown1.Value + numericUpDown2.Value
                        + numericUpDown3.Value + numericUpDown4.Value 
                        + numericUpDown5.Value;

            calculo = calculo / 5;

            string temp = "Resultado : ";

            if(calculo >= 70)
            {
                label6.ForeColor = Color.Green;
                label6.Text = temp + "Aprobado";
                return; 
            }

            label6.ForeColor = Color.Red;
            label6.Text = temp + "Reprobado";

        }
    }
}
