﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Prac23
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Regex r = new Regex("^[a-zA-Z0-9]*$");
            if (!r.IsMatch(textBox1.Text))
            {
                MessageBox.Show("El nombre de Usuario puede tener solo letras y numeros");
                return;

            }

            if (textBox1.Text.Length < 6)
            {
                MessageBox.Show("El nombre de usuario debe contener al " + 
                "menos 6 caracteres");
                return;
            }

            if(textBox1.Text.Length > 12)
            {
                MessageBox.Show("El nombre de usuario no puede contener un " +
                "un maximo de 12.");
                return;

            }
        }
    }
}
