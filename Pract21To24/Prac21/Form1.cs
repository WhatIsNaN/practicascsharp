﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac21
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox1.BackgroundImage = Image.FromFile("Logo.png");
            pictureBox1.BackgroundImageLayout = ImageLayout.Stretch;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Invoice.Factura.Clear();
            Invoice newFactura = new Invoice();
            double LitrosGastados = Convert.ToDouble(numericUpDown1.Value);
            
            if(LitrosGastados == 0)
            {
                MessageBox.Show("Porfavor, asegurese de consultar un numero especifico de litros.");
                return;
            }

            double Cobro = 0;

            Cobro += 500; //cuota fija mensual;
                       
            if (LitrosGastados > 50)
            {
                double PrimerosLitros = 50; //oferta gratis 50 primeros litros
                double PrimeraTarifa = 150; //del 50 al 200 se cobran 1;

                LitrosGastados = LitrosGastados - PrimerosLitros;

                if ((LitrosGastados + PrimerosLitros) > 200)
                {
                    LitrosGastados = LitrosGastados - PrimeraTarifa;

                    PrimerosLitros = 0;
                    PrimeraTarifa = PrimeraTarifa * 1.0;
                    LitrosGastados = LitrosGastados * 3.0;

                    Cobro = Cobro + PrimerosLitros + PrimeraTarifa + LitrosGastados;
                    textBox1.Text = Cobro.ToString();
                    Invoice.Factura.Add("Cuota Fija: ", 500);
                    Invoice.Factura.Add("Primeros 50 Litros Gratis :", PrimerosLitros);
                    Invoice.Factura.Add("Cobro a 1.00$RD de 50 a 200 Litros :", PrimerosLitros) ;
                    Invoice.Factura.Add("Cobro a 3.00$RD por encima de 200 Litros", LitrosGastados * 3);
                    newFactura.Show();
                    return;
                }

                PrimerosLitros = 0;
                LitrosGastados = LitrosGastados * 1.0;
                Cobro = Cobro + PrimerosLitros + LitrosGastados;
                textBox1.Text = Cobro.ToString();
                Invoice.Factura.Add("Cuota Fija: ", 500);
                Invoice.Factura.Add("Primeros 50 Litros Gratis :", PrimerosLitros);
                Invoice.Factura.Add("Cobro a 1.00$RD de 50 a 200 Litros :", LitrosGastados);
                newFactura.Show();
                return;
            }
            textBox1.Text = Cobro.ToString();
            Invoice.Factura.Add("Cuota Fija: ", 500);
            Invoice.Factura.Add("Primeros 50 Litros Gratis :", 0);
            newFactura.Show();
        }
    }
}
