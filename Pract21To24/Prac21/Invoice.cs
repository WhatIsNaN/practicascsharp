﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac21
{   
    public partial class Invoice : Form
    {
        static public Dictionary<string, double> Factura = new Dictionary<string, double>();

        public Invoice()
        {
            InitializeComponent();
        }

        private void Invoice_Load(object sender, EventArgs e)
        {
            listBox1.Items.AddRange(Factura.Keys.ToArray());
            listBox2.Items.AddRange(Factura.Values.Cast<object>().ToArray());
            label2.Text = DateTime.Now.ToString();
            listBox1.Items.Add("----------");
            listBox2.Items.Add("----------");
            listBox1.Items.Add("Total :");
            listBox2.Items.Add(SumArray());
        }

        public double SumArray()
        {
            double TotalValue = 0;
            foreach(double value in Factura.Values)
            {
                TotalValue += value;
            }
            return TotalValue;
        }

        private void Invoice_FormClosing(object sender, FormClosingEventArgs e)
        {
            Factura.Clear();
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
