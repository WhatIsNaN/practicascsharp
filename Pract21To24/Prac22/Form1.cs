﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac22
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                textBox4.Text = textBox1.Text.Substring(0, 2) + comboBox1.Text + maskedTextBox1.Text.Substring(1, 3);
                double resultado = 0;

                foreach (var item in checkedListBox1.CheckedItems)
                {
                    if (item.ToString() == "Empleado Fijo")
                    {
                        resultado += 2;
                    }
                    else
                    {
                        resultado += 1;
                    }

                }

                resultado = (resultado * 100) / 10;
                textBox2.Text = resultado.ToString() + "%";
            }
            catch
            {
                MessageBox.Show("Valores Invalidos");
            }

        }
    }
}
