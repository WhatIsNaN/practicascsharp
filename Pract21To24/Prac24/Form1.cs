﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prac24
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string password = textBox1.Text;
            bool valida;
            if (password.Length < 8)
            {
                MessageBox.Show("la Contraseña debe contener un minimo de 8 caracteres");
            }
            bool upperCase = false;
            bool lowerCase = false;
            bool CaracterNoAlfa = false;
            int NoCaracterNoAlfa = 0;
            bool numero = false;

            if (!(password == password.Trim()))
            {
                MessageBox.Show("La Contraseña no permite espacios en blanco");
            }

            foreach (char letra in password.ToCharArray())
            {
                if (char.IsUpper(letra))
                {
                    upperCase = true;
                }

                if (char.IsLower(letra))
                {
                    lowerCase = true;
                }

                if (char.IsNumber(letra))
                {
                    numero = true;
                }

                if (char.IsSymbol(letra) || char.IsPunctuation(letra))
                {
                    if (NoCaracterNoAlfa == 1)
                    {
                        CaracterNoAlfa = true;
                    }
                    else
                    {
                        NoCaracterNoAlfa += 1;
                    }
                }
            }
            if (upperCase && lowerCase && CaracterNoAlfa & numero)
            {
                MessageBox.Show("Contraseña Valida");
            }
            else
            {
                MessageBox.Show("Contraseña No Valida");

            }
        }
        }
    }
